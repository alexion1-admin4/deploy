/*
@Author :chaitra swamy
@Name: AIP_CPEP_BeforeInsert_HandlerTest 
@CreatedDate: 23-Dec-2020
@Description: Test class for AIP_CPEP_BeforeInsert_Handler.
@version : 1.0  
*/
@isTest(seeAllData=false)
public with sharing class AIP_CPEP_BeforeInsert_HandlerTest {
    Private static User thisUser = [select Id from User where Id = :UserInfo.getUserId() limit 1];
    Private Id integrationProfile=[select id from profile where name='Integration'].id;
     Private User integrationUser = new User(alias = 'test123', email='test123@noemail.com',
        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
        localesidkey='en_US', profileid = integrationProfile, country='United States',IsActive =true,
        timezonesidkey='America/Los_Angeles', username='testuser@noemaill.com'); 
	/*Constructor*/
    static void AIP_CPEP_BeforeInsert_HandlerTest(){
        System.runAs(thisUser){System.assert(true, 'CPEP Insert');}

    }
    /*Method called to create Account */
    @testSetup static void createDataForAccount()
    {
        APS_DataUtility dataUtilityObj = new APS_DataUtility();
       
    	Account patientAccount = dataUtilityObj.createpatientAccount();
           insert patientAccount;
   
            Account hcpAccountObj = dataUtilityObj.createHCPAccount();
            insert hcpAccountObj;
     
        CareProgram programObj = dataUtilityObj.createProgram('Gevera Program');
                insert programObj;
        
        CareProgramEnrollee programAffObj = dataUtilityObj.createProgramAff(patientAccount, hcpAccountObj, programObj);
                database.insert(programAffObj,true);
            //Date svalue = Date.newInstance(2020, 5, 7);
            //Date dvalue = Date.newInstance(2020, 5, 12);
            
        Product2 productObj = dataUtilityObj.createProduct('Gevera');
            database.insert(productObj,true);

        CareProgramProduct prgPrdAffObj = dataUtilityObj.createPrgPrdAff(programObj, productObj);
            prgPrdAffObj.Name = 'Gevera';
            database.insert(prgPrdAffObj,true);
        
       Id OrderOnlyRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AIP_Order_Only').getRecordTypeId();
       Account accountObj = new Account(
                FirstName='fname',
                LastName='lname',
            	AIP_Off_label_Flag__c='On-Label',
            	AIP_Indication__c='HPP',
                RecordTypeId=OrderOnlyRecordTypeId);
       database.insert(accountObj,true);
        
           
   
    }
    
    /*Method called to test the throwErrorForDuplicateCPEP Insert */
    @isTest static void throwErrorForDuplicateCPEPTest()
    {
        System.runAs(thisUser){  
            List<CareProgramEnrolleeProduct> cpepList=new List<CareProgramEnrolleeProduct>();
            CareProgramEnrollee programAffObj=[SELECT Id,Name FROM CareProgramEnrollee Limit 1];
            CareProgramProduct prgPrdAffObj=[SELECT Id,Name FROM CareProgramProduct Limit 1];
            CareProgramEnrolleeProduct CPEP= new CareProgramEnrolleeProduct(Name='Test CPEP',CareProgramEnrolleeId=programAffObj.id,CareProgramProductId =prgPrdAffObj.id );
        	database.insert(CPEP,true);
            cpepList.add(CPEP);
            Test.StartTest();
            	AIP_CPEP_BeforeInsert_Handler.throwErrorForDuplicateCPEP(cpepList);
            Test.StopTest();
            System.assert(true, 'throwErrorForDuplicateCPEP Insert');
        }
        
    }
    
     /*Method called to test the CPEP Insert */
    @isTest static void CPEPInsertTest()
    {
        System.runAs(thisUser){  
            Id orderRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AIP_Order_Only').getRecordTypeId();
            Account orderAccount=[Select Id,AIP_Primary_HCP__c FROM Account WHERE RecordtypeId=:orderRecordTypeId  Limit 1];
            CareProgramEnrollee programAffObj=[SELECT Id,Name FROM CareProgramEnrollee Limit 1];
            CareProgramProduct prgPrdAffObj=[SELECT Id,Name FROM CareProgramProduct Limit 1];
            Test.StartTest();
            	CareProgramEnrolleeProduct CPEP= new CareProgramEnrolleeProduct(Name='Test CPEP',
                                                                                CareProgramEnrolleeId=programAffObj.id,
                                                                                CareProgramProductId =prgPrdAffObj.id,
                                                                                AIP_Account__c=orderAccount.id,
                                                                                AIP_Therapy_Start_Date__c=date.today());
        		database.insert(CPEP,true);
            Test.StopTest();
            System.assert(true, 'CPEP Insert');
        }
        
    }
    
    /*Method called to test the throwErrorForDuplicateCPEP Update */
    @isTest static void throwErrorForDuplicateCPEPTestUpdate()
    {
            Id integrationProfile=[select id from profile where name='Integration'].id;
          	User integrationUser = [select id,profileid from User where profileid =: integrationProfile];
            APS_DataUtility dataUtilityObj = new APS_DataUtility();
            List<CareProgramEnrolleeProduct> cpepList=new List<CareProgramEnrolleeProduct>();
            CareProgramEnrollee programAffObj=[SELECT Id,Name FROM CareProgramEnrollee Limit 1];
            CareProgramProduct prgPrdAffObj=[SELECT Id,Name FROM CareProgramProduct Limit 1];
            CareProgramEnrolleeProduct CPEP= new CareProgramEnrolleeProduct(Name='Test CPEP',CareProgramEnrolleeId=programAffObj.id,CareProgramProductId =prgPrdAffObj.id );
        	database.insert(CPEP,true);
            cpepList.add(CPEP);
            CareProgram programObj = dataUtilityObj.createProgram('PNH');
                insert programObj;
            Product2 productObj = dataUtilityObj.createProduct('Soliris');
            database.insert(productObj,true);
        System.runAs(thisUser){
            CareProgramProduct prgPrdAffObj2 = dataUtilityObj.createPrgPrdAff(programObj, productObj);
            prgPrdAffObj2.Name = 'Soliris';
            database.insert(prgPrdAffObj2,true);
            Test.StartTest();
            	//AIP_CPEP_BeforeInsert_Handler.throwErrorForDuplicateCPEP(cpepList);
            	//cpep.CareProgramProductId=prgPrdAffObj2.id;
            	cpep.AIP_First_Shipment_Date__c=Date.today();
            	cpep.Status='Treatment Coordination';
            	cpep.AIP_Status_Reason__c='Shipped';
            	Database.update(cpep);
            	cpep.Status='On Therapy';
            	cpep.AIP_Status_Reason__c=null;
            	Database.update(cpep);
            Test.StopTest();
            System.assert(true, 'throwErrorForDuplicateCPEP Insert');
        }
        
    }
     /*Method called to test the throwErrorForDuplicateCPEP Update */
    @isTest static void throwErrorForDuplicateCPEPTestUpdate2()
    {
            Id integrationProfile=[select id from profile where name='Integration'].id;
          	User integrationUser = [select id,profileid from User where profileid =: integrationProfile];
            APS_DataUtility dataUtilityObj = new APS_DataUtility();
            List<CareProgramEnrolleeProduct> cpepList=new List<CareProgramEnrolleeProduct>();
            CareProgramEnrollee programAffObj=[SELECT Id,Name FROM CareProgramEnrollee Limit 1];
            CareProgramProduct prgPrdAffObj=[SELECT Id,Name FROM CareProgramProduct Limit 1];
            CareProgramEnrolleeProduct CPEP= new CareProgramEnrolleeProduct(Name='Test CPEP',CareProgramEnrolleeId=programAffObj.id,CareProgramProductId =prgPrdAffObj.id );
        	database.insert(CPEP,true);
            cpepList.add(CPEP);
            CareProgram programObj = dataUtilityObj.createProgram('PNH');
                insert programObj;
            Product2 productObj = dataUtilityObj.createProduct('Soliris');
            database.insert(productObj,true);
        System.runAs(thisUser){
            CareProgramProduct prgPrdAffObj2 = dataUtilityObj.createPrgPrdAff(programObj, productObj);
            prgPrdAffObj2.Name = 'Soliris';
            database.insert(prgPrdAffObj2,true);
            Test.StartTest();
            	//AIP_CPEP_BeforeInsert_Handler.throwErrorForDuplicateCPEP(cpepList);
            	//cpep.CareProgramProductId=prgPrdAffObj2.id;
            	cpep.AIP_First_Shipment_Date__c=Date.today();
            	cpep.Status='Treatment Coordination';
            	cpep.AIP_Status_Reason__c='Shipped';
            	Database.update(cpep);
            	cpep.Status='On Therapy';
            	cpep.AIP_Status_Reason__c=null;
            	Database.update(cpep);
            	cpep.status='On Hold';
            	cpep.AIP_status_reason__c='Other';
            	Database.update(cpep);
            Test.StopTest();
            System.assert(true, 'throwErrorForDuplicateCPEP Insert');
        }
        
    }
}