@isTest(seeAllData=false)
private with sharing class AIP_CaseAftUpdTriggerHandlerTest{
    Private static User thisUser = [select Id from User where Id = :UserInfo.getUserId() limit 1];
    
    /*Method to test the caseValidationCareplan method in AIP_CaseBeforeUpdateTriggerHandler*/   
    @isTest static void caseValidationCareplantest(){
        system.debug('@@@@@@@ insidecaseValidationCareplantest ');
        System.runAs(thisUser){ 
            
            APS_DataUtility dataUtilityObj = new APS_DataUtility();
            dataUtilityObj.insertingCustomSettingData();
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            
            List<Account> accList = new List<Account>();
            Account patientAccount = dataUtilityObj.createpatientAccount();
           insert patientAccount;
              APS_Task__c tsk =new APS_Task__c();
          //  tsk.APS_Case__c = csObj.Id;
            Id tskRecordType=Schema.SObjectType.APS_Task__c.getRecordTypeInfosByDeveloperName().get('AIP_Assess_Risk').getRecordTypeId(); 
           
            tsk.RecordTypeId=tskRecordType;
            tsk.APS_Patient__c=patientAccount.Id;
            tsk.APS_Status__c = 'Assigned';
          //  tsk.APS_Problem__c = Problem.id;
          //  listTsk.add(tsk);
           // Database.insert(listTsk);
         //  insert tsk;   
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            Account hcpAccountObj = dataUtilityObj.createHCPAccount();
               insert hcpAccountObj;
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            APS_Zip_Code_Master__c zipCodeMasterObj = dataUtilityObj.createZipCode('60690');
            Database.insert(zipCodeMasterObj,true);
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            CareProgram programObj = dataUtilityObj.createProgram('HPP');
            Database.insert(programObj,true);
             CareProgram programObj2 = dataUtilityObj.createProgram('aHUS');
            Database.insert(programObj2,true);
            Product2 productObj = dataUtilityObj.createProduct('Strensiq');
            Database.insert(productObj,true);
             CareProgramEnrollee cpe = new CareProgramEnrollee();
            cpe.AccountId=patientAccount.id;
            cpe.AIP_Active__c=true;
                cpe.CareProgramId=programObj2.id;
                insert cpe;
              system.debug('@@@@@@@cpe insidecaseValidationCareplantest cpe '+cpe);
            CareProgramEnrollee prgAff1 = dataUtilityObj.createProgramAff(patientAccount,hcpAccountObj,programObj);
            Database.insert(prgAff1,true);
            
            CareProgramProduct prgPrdAffObj = dataUtilityObj.createPrgPrdAff(programObj, productObj);
            prgPrdAffObj.Name = 'Strensiq';
            Database.insert(prgPrdAffObj,true);
            
              system.debug('@@@@@@@ insidecaseValidationCareplantest prgPrdAffObj '+prgPrdAffObj);
            
            APS_Workflow_Master__c workflowMasterObj = new APS_Workflow_Master__c(
                Name = 'CarePlan',
                APS_Release_Start_Date__c = Date.Today()-10,
                APS_Release_End_Date__c = Date.Today()+10,
                APS_Workflow_Type__c = 'Adherence',
                APS_Description__c = 'Hpp',
                APS_Record_Type__c = 'CAREPLAN'
            );
            Database.insert(workflowMasterObj,true);
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            APS_Program_Product_WF_Aff__c prgPrdWFAffObj = new APS_Program_Product_WF_Aff__c(
                Name = 'Hpp',
                APS_Workflow__c = workflowMasterObj.Id,
                APS_Care_Program_Product_Aff__c = prgPrdAffObj.Id
            );
            Database.insert(prgPrdWFAffObj,true);
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            APS_Program_Product_WF_Version__c prgPrdWFVer = new APS_Program_Product_WF_Version__c(
                APS_Program_Product_WF_Aff__c = prgPrdWFAffObj.Id,
                APS_Release_End_Date__c = Date.Today()+10,
                APS_Release_Start_Date__c =Date.Today()-10,
                Name='Version 1'    
            );
            Database.insert(prgPrdWFVer,true);
             system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            
            
       //     Case csObj = dataUtilityObj.createCase(patientAccount, prgAff1, hcpAccountObj, programObj, prgPrdAffObj,'CarePlan');
               Case csObj = dataUtilityObj.createCase_2(patientAccount, prgAff1, hcpAccountObj, programObj, prgPrdAffObj,'CarePlan');
             csObj.APS_Care_Program_Product_Aff__c = prgPrdAffObj.Id;
              system.debug('@@@@@@@ insidecaseValidationCareplantest csObj : '+csObj);
           // csObj.Status = 'Open';
        //    csObj.APS_Account__c = patientAccount.id; 
        //  
         Test.startTest();

            Database.insert(csObj,true);
                tsk.APS_Case__c = csObj.Id;
           insert tsk;
          
            system.debug('@@@@@@@ insidecaseValidationCareplantest csObj : '+csObj);
            HealthCloudGA__CarePlanProblem__c Problem = New HealthCloudGA__CarePlanProblem__c ();
            Problem.Name = 'Problem 1';
            Problem.HealthCloudGA__CarePlan__c = csObj.id;
            Problem.APS_Careplan_Visualization_Flag__c = true;
            insert Problem;
            
           List<APS_Task__c> listTsk=new List<APS_Task__c>();
         //   APS_Task__c tsk =dataUtilityObj.createTask('Assess Risk');
          
              system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.APS_Care_Program_Enrollee__c '+ csObj.APS_Care_Program_Enrollee__c);
            system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.Status '+ csObj.Status);
            system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.AIP_Status_Reason__c '+ csObj.AIP_Status_Reason__c);
           
            csObj.APS_Care_Program_Enrollee__c =cpe.id;
            csObj.AIP_Careplan_Case_Created__c = 'yes';        
            csObj.AIP_Status_Reason__c = 'Completed_test';
            csObj.Status = 'Closed';
            system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.APS_Care_Program_Enrollee__c '+ csObj.APS_Care_Program_Enrollee__c);
          system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.Status '+ csObj.Status);
            system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.AIP_Status_Reason__c '+ csObj.AIP_Status_Reason__c);
          
  
           Database.SaveResult result = Database.Update(csObj, false); 
           
            AIP_CaseAfterUpdateTriggerHandler.cancelAllPendingTasks(null);
            Test.stopTest();
           // System.assertEquals(Label.AIP_CaseCloseValidation,result.getErrors()[0].getMessage());     
              
        }
    }
    
    
    /*Method to test the caseValidationCareplan method in AIP_CaseBeforeUpdateTriggerHandler*/   
    @isTest static void cpeTaskUpdate(){
        system.debug('@@@@@@@ insidecaseValidationCareplantest ');
        System.runAs(thisUser){ 
            
            APS_DataUtility dataUtilityObj = new APS_DataUtility();
            dataUtilityObj.insertingCustomSettingData();
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            
            List<Account> accList = new List<Account>();
            Account patientAccount = dataUtilityObj.createpatientAccount();
           insert patientAccount;
              APS_Task__c tsk =new APS_Task__c();
          //  tsk.APS_Case__c = csObj.Id;
            Id tskRecordType=Schema.SObjectType.APS_Task__c.getRecordTypeInfosByDeveloperName().get('AIP_Assess_Risk').getRecordTypeId(); 
           
            tsk.RecordTypeId=tskRecordType;
            tsk.APS_Patient__c=patientAccount.Id;
            tsk.APS_Status__c = 'Assigned';
          //  tsk.APS_Problem__c = Problem.id;
          //  listTsk.add(tsk);
           // Database.insert(listTsk);
         //  insert tsk;   
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            Account hcpAccountObj = dataUtilityObj.createHCPAccount();
               insert hcpAccountObj;
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            APS_Zip_Code_Master__c zipCodeMasterObj = dataUtilityObj.createZipCode('60690');
            Database.insert(zipCodeMasterObj,true);
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            CareProgram programObj = dataUtilityObj.createProgram('HPP');
            Database.insert(programObj,true);
             CareProgram programObj2 = dataUtilityObj.createProgram('aHUS');
            Database.insert(programObj2,true);
            Product2 productObj = dataUtilityObj.createProduct('Strensiq');
            Database.insert(productObj,true);
             CareProgramEnrollee cpe = new CareProgramEnrollee();
            cpe.AccountId=patientAccount.id;
            cpe.AIP_Active__c=true;
                cpe.CareProgramId=programObj2.id;
                insert cpe;
              system.debug('@@@@@@@cpe insidecaseValidationCareplantest cpe '+cpe);
            CareProgramEnrollee prgAff1 = dataUtilityObj.createProgramAff(patientAccount,hcpAccountObj,programObj);
            Database.insert(prgAff1,true);
            
            CareProgramProduct prgPrdAffObj = dataUtilityObj.createPrgPrdAff(programObj, productObj);
            prgPrdAffObj.Name = 'Strensiq';
            Database.insert(prgPrdAffObj,true);
            
              system.debug('@@@@@@@ insidecaseValidationCareplantest prgPrdAffObj '+prgPrdAffObj);
            
            APS_Workflow_Master__c workflowMasterObj = new APS_Workflow_Master__c(
                Name = 'CarePlan',
                APS_Release_Start_Date__c = Date.Today()-10,
                APS_Release_End_Date__c = Date.Today()+10,
                APS_Workflow_Type__c = 'Adherence',
                APS_Description__c = 'Hpp',
                APS_Record_Type__c = 'CAREPLAN'
            );
            Database.insert(workflowMasterObj,true);
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            APS_Program_Product_WF_Aff__c prgPrdWFAffObj = new APS_Program_Product_WF_Aff__c(
                Name = 'Hpp',
                APS_Workflow__c = workflowMasterObj.Id,
                APS_Care_Program_Product_Aff__c = prgPrdAffObj.Id
            );
            Database.insert(prgPrdWFAffObj,true);
              system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            APS_Program_Product_WF_Version__c prgPrdWFVer = new APS_Program_Product_WF_Version__c(
                APS_Program_Product_WF_Aff__c = prgPrdWFAffObj.Id,
                APS_Release_End_Date__c = Date.Today()+10,
                APS_Release_Start_Date__c =Date.Today()-10,
                Name='Version 1'    
            );
            Database.insert(prgPrdWFVer,true);
             system.debug('@@@@@@@ insidecaseValidationCareplantest ');
            
            
       //     Case csObj = dataUtilityObj.createCase(patientAccount, prgAff1, hcpAccountObj, programObj, prgPrdAffObj,'CarePlan');
               Case csObj = dataUtilityObj.createCase_2(patientAccount, prgAff1, hcpAccountObj, programObj, prgPrdAffObj,'CarePlan');
             csObj.APS_Care_Program_Product_Aff__c = prgPrdAffObj.Id;
              system.debug('@@@@@@@ insidecaseValidationCareplantest csObj : '+csObj);
           // csObj.Status = 'Open';
        //    csObj.APS_Account__c = patientAccount.id; 
        //  
         Test.startTest();

            Database.insert(csObj,true);
                tsk.APS_Case__c = csObj.Id;
           insert tsk;
          
            system.debug('@@@@@@@ insidecaseValidationCareplantest csObj : '+csObj);
            HealthCloudGA__CarePlanProblem__c Problem = New HealthCloudGA__CarePlanProblem__c ();
            Problem.Name = 'Problem 1';
            Problem.HealthCloudGA__CarePlan__c = csObj.id;
            Problem.APS_Careplan_Visualization_Flag__c = true;
            insert Problem;
            
           List<APS_Task__c> listTsk=new List<APS_Task__c>();
         //   APS_Task__c tsk =dataUtilityObj.createTask('Assess Risk');
          
              system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.APS_Care_Program_Enrollee__c '+ csObj.APS_Care_Program_Enrollee__c);
            system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.Status '+ csObj.Status);
            system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.AIP_Status_Reason__c '+ csObj.AIP_Status_Reason__c);
           
            csObj.APS_Care_Program_Enrollee__c =cpe.id;
            csObj.AIP_Careplan_Case_Created__c = 'yes';        
            csObj.AIP_Status_Reason__c = 'Completed_test';
           // csObj.Status = 'Closed';
            system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.APS_Care_Program_Enrollee__c '+ csObj.APS_Care_Program_Enrollee__c);
          system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.Status '+ csObj.Status);
            system.debug('@@@@@@@ insidecaseValidationCareplantest csObj.AIP_Status_Reason__c '+ csObj.AIP_Status_Reason__c);
          
  
           Database.SaveResult result = Database.Update(csObj, false); 
           
          //  AIP_CaseAfterUpdateTriggerHandler.cpeTaskUpdate(null);
            Test.stopTest();
           // System.assertEquals(Label.AIP_CaseCloseValidation,result.getErrors()[0].getMessage());     
              
        }
    }
      

}