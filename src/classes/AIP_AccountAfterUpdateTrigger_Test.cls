/**
@Author :
@Name: AIP_AccountAfterUpdateTrigger_Test 
@CreatedDate: 
@Description: AIP_AccountAfterUpdateTriggerHandler 
@version : 1.0  
Hi + hello
*/
@isTest(seeAllData=false)
public with sharing class AIP_AccountAfterUpdateTrigger_Test {
    Private static User thisUser = [select Id from User where Id = :UserInfo.getUserId()limit 1];
    /*Constructor*/
    static void AIP_AccountAfterUpdateTrigger_Test(){
        System.runAs(thisUser){System.assert(true, 'AIP_AccountAfterUpdateTrigger_Test');}
    }
        /*Method called to create Account */
    @testSetup static void createDataForAccount()
    {
        APS_DataUtility dataUtilityObj = new APS_DataUtility();
       
    	   Account patientAccount = dataUtilityObj.createpatientAccount();
           insert patientAccount;
   
            Account hcpAccountObj = dataUtilityObj.createHCPAccount();
            insert hcpAccountObj;
        
           Id OrderOnlyRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AIP_Order_Only').getRecordTypeId();
           Account accountObj = new Account(
                    FirstName='fname',
                    LastName='lname',
                    AIP_Off_label_Flag__c='Off-Label, by age only',
                    AIP_Indication__c='HPP',
                    RecordTypeId=OrderOnlyRecordTypeId);
           database.insert(accountObj,true);
        
        CareProgram programObj = dataUtilityObj.createProgram('Gevera Program');
                insert programObj;
        
        CareProgramEnrollee programAffObj = dataUtilityObj.createProgramAff(accountObj, hcpAccountObj, programObj);
        programAffObj.AIP_Off_Label_Flag__c='test';
                database.insert(programAffObj,true);
           // Date svalue = Date.newInstance(2020, 5, 7);
           // Date dvalue = Date.newInstance(2020, 5, 12);
            
        Product2 productObj = dataUtilityObj.createProduct('Soliris');
            database.insert(productObj,true);

        CareProgramProduct prgPrdAffObj = dataUtilityObj.createPrgPrdAff(programObj, productObj);
            prgPrdAffObj.Name = 'Soliris';
            database.insert(prgPrdAffObj,true);
        APS_Health_Care_Professional_Affiliation__c hcpa = new APS_Health_Care_Professional_Affiliation__c();
        hcpa.APS_Patient__c=accountObj.Id;
        hcpa.APS_HCP__c=hcpAccountObj.Id;
        hcpa.AIP_Relationship_Type__c='Treating';
        hcpa.AIP_Active__c=TRUE;
        Database.insert(hcpa,true);
 		
    }
    @istest static void createDataForTask()
    {
        System.runAs(thisUser){
        Id recordTypeId2 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Health Care Professionals').getRecordTypeId();
        Account hcpacc1 = new Account(
            FirstName='Gandalf1',
            LastName='Grey1',
            RecordTypeId=recordTypeId2,
            AIP_Account_Status__c='Valid',
            AIP_Veeva_External_ID__c = 'WIN1',
            AIP_No_Contact__c = true);
        Database.insert(hcpacc1);
        
        Id OORecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Order Only').getRecordTypeId();
        Account accountObj1 = new Account(
            FirstName='fname',
            LastName='lname',
            AIP_Off_label_Flag__c='On-Label',
            AIP_Indication__c='HPP',
            AIP_Has_Been_On_Therapy__c=false,
            RecordTypeId=OORecordTypeId);
            database.insert(accountObj1,true);
        
        Id PatRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Patient').getRecordTypeId();
        Account accountObj2 = new Account(
            FirstName='fname',
            LastName='lname',
            AIP_Off_label_Flag__c='On-Label',
            AIP_Indication__c='HPP',
            AIP_Has_Been_On_Therapy__c=false,
            RecordTypeId=PatRecordTypeId);
            database.insert(accountObj2,true);
            
        Id tskrecordTypeId1 = Schema.SObjectType.APS_Task__C.getRecordTypeInfosByName().get('Send Fax').getRecordTypeId();    
        APS_Task__c task = new APS_Task__c();
        task.APS_Patient__c = accountObj1.id;
        task.AIP_Support_Type__c = 'Copay';
        task.RecordTypeId = tskrecordTypeId1;
        task.APS_Status__c = 'Assigned';
        Database.insert(task);
               
        APS_Task__c task1 = new APS_Task__c();
        task1.APS_Patient__c = accountObj2.id;
        task1.AIP_Support_Type__c = 'Copay';
        task1.RecordTypeId = tskrecordTypeId1;
        task1.APS_Status__c = 'Not Assigned';
        Database.insert(task1);

        List< Account > AccUpdatelist = New List< Account > ();
        accountObj1.AIP_Primary_HCP__c = hcpacc1.id;
        AccUpdatelist.add(accountObj1);
        
        accountObj2.AIP_Primary_HCP__c = hcpacc1.id;
        AccUpdatelist.add(accountObj2);
        
        Database.Update(AccUpdatelist);
       
        system.assert(task1.Id !=NULL);
    }
  }
    @isTest static void accUpdateTest()
    {
        System.runAs(thisUser){
            Id orderRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AIP_Order_Only').getRecordTypeId();
            Id patientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('APS_Patients').getRecordTypeId();
            Account orderAccount=[Select Id,AIP_Primary_HCP__c,RecordTypeId,AIP_Off_label_Flag__c FROM Account WHERE RecordtypeId=:orderRecordTypeId  Limit 1];
            CareProgramEnrollee programAffObj=[SELECT Id,Name FROM CareProgramEnrollee Limit 1];
            CareProgramProduct prgPrdAffObj=[SELECT Id,Name FROM CareProgramProduct Limit 1];
            CareProgramEnrolleeProduct CPEP= new CareProgramEnrolleeProduct(Name='Test CPEP',
                                                                                CareProgramEnrolleeId=programAffObj.id,
                                                                                CareProgramProductId =prgPrdAffObj.id,
                                                                                Status='Treatment Coordination',
                                                                                AIP_Account__c=orderAccount.id);
            Database.insert(cpep);
            Test.startTest();
            orderAccount.RecordtypeId=patientRecordTypeId;
            orderAccount.AIP_Indication__c='AHUS';
            Database.update(orderAccount);
            Test.stopTest();
            
            system.assert(orderAccount.Id!=NULL);
        }
    }
        @isTest static void accOffLabelTest()
    {
        System.runAs(thisUser){
            Id orderRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AIP_Order_Only').getRecordTypeId();
            Account orderAccount=[Select Id,AIP_Primary_HCP__c,RecordTypeId,AIP_Off_label_Flag__c FROM Account WHERE RecordtypeId=:orderRecordTypeId  Limit 1];
            orderAccount.AIP_Indication__c = 'aHUS';
            update orderAccount;
            CareProgramEnrollee programAffObj=[SELECT Id,Name FROM CareProgramEnrollee Limit 1];
            CareProgramProduct prgPrdAffObj=[SELECT Id,Name FROM CareProgramProduct Limit 1];
            CareProgramEnrolleeProduct CPEP= new CareProgramEnrolleeProduct(Name='Test CPEP',
                                                                                CareProgramEnrolleeId=programAffObj.id,
                                                                                CareProgramProductId =prgPrdAffObj.id,
                                                                                Status='Treatment Coordination',
                                                                                AIP_Account__c=orderAccount.id);
            Database.insert(cpep);
            Test.startTest();
            orderAccount.AIP_Off_Label_Flag__c='On-Label';
            
            Database.update(orderAccount);
            Test.stopTest();
            
            system.assert(orderAccount.Id!=NULL);
        }
     }
}