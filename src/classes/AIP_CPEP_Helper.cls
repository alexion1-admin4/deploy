/*
************************************************************************************************************************************
@Class Name: AIP_CPEP_Helper
@description : This is class used to handle all computations with respect to CPEP triggers
@date :  20-Nov-2020
Hello World
*************************************************************************************************************************************/
public without sharing class AIP_CPEP_Helper { 
    private static final string CPEPUPDATEOPERATIONS ='cpepUpdateOperations';
    private static final string CLASSNAME='AIP_CPEP_Helper';
    private static final string UPDATEPOCONACCOUNT ='updatePOConAccountonCPEPInsert';
    private static final string CREATEOUTREACHTASKS ='createOutreachTasks';
    private static final string UPDATEPOC ='updatePOC';

    /*
************************************************************************************************************************************
@Method Name: cpepUpdateOperations
@description : This method will handle all CPEP updated operation calls
@date :  20-Nov-2020
*************************************************************************************************************************************/
    public static void cpepUpdateOperations(List<CareProgramEnrolleeProduct> newCPEPList,Map<Id,CareProgramEnrolleeProduct> oldCPEPMap)
    {
        Set<String> accIdSet = new Set<String>();
        //List<CareProgramEnrolleeProduct> cpepOutreachList = new List<CareProgramEnrolleeProduct>();
        Set<Id> outreachSiteIdSet = new Set<Id>();
        Map<Id,String> cpepIdStringMap=new Map<Id,String>();
        String OFFLABEL='Off-Label';
        String ONLABEL='On-Label';
        Map<Id,CareProgramEnrolleeProduct> offLabelCPEP = new  Map<Id,CareProgramEnrolleeProduct>();
        Map<Id,CareProgramEnrolleeProduct> onLabelCPEP = new  Map<Id,CareProgramEnrolleeProduct>();
        List<Order> ordList=new List<Order>();
        List<Order> updateList=new List<Order>();
        try{
        for(CareProgramEnrolleeProduct cpepObj : newCPEPList)
        {
            if(oldCPEPMap.get(cpepObj.Id).Status != System.Label.AIP_On_Therapy && cpepObj.Status == System.Label.AIP_On_Therapy)
            {
                accIdSet.add(cpepObj.AIP_Account__c);   
                if(cpepObj.AIP_Ship_To__c!=NULL && (String.isBlank(cpepObj.AIP_Off_Label_Flag__c)||(!String.isBlank(cpepObj.AIP_Off_Label_Flag__c) && cpepObj.AIP_Off_Label_Flag__c == System.Label.AIP_On_Label)))
                {
                      outreachSiteIdSet.add(cpepObj.AIP_Ship_To__c); 
                }
            } 
            //adding for Off labl by age to On Label switch scenario
            else if( !('On-Label').equals(oldCPEPMap.get(cpepObj.ID).AIP_Off_Label_Flag__c) && (('On-Label').equals(cpepObj.AIP_Off_Label_Flag__c)) && (System.Label.AIP_On_Therapy.equals(cpepObj.Status)))
            {
                accIdSet.add(cpepObj.AIP_Account__c);  
            }
            if((String.isBlank(cpepObj.AIP_Off_Label_Flag__c)||(!String.isBlank(cpepObj.AIP_Off_Label_Flag__c)&& cpepObj.AIP_Off_Label_Flag__c == System.Label.AIP_On_Label))
               && (oldCPEPMap.get(cpepObj.Id).AIP_Ship_To__c != cpepObj.AIP_Ship_To__c) && cpepObj.Status == System.Label.AIP_On_Therapy)
            {
                // cpepOutreachList.add(cpepObj);
                outreachSiteIdSet.add(cpepObj.AIP_Ship_To__c); 
            }
            
            if(cpepObj.AIP_Off_Label_Flag__c!=oldCPEPMap.get(cpepObj.Id).AIP_Off_Label_Flag__c){
            	cpepIdStringMap.put(cpepObj.Id,cpepObj.AIP_Off_Label_Flag__c);
            }
        }
        
        
        if(!cpepIdStringMap.keyset().isEmpty()){
            for(CareProgramEnrolleeProduct cpep:[Select Id,CareProgramEnrollee.CareProgram.Name from CareProgramEnrolleeProduct  
                                                 where Id IN:cpepIdStringMap.keyset() LIMIT 49999]){
                        if((cpepIdStringMap.get(cpep.Id)!=NULL)&&(cpepIdStringMap.get(cpep.Id).contains(OFFLABEL))){
                                   offLabelCPEP.put(cpep.Id,cpep);                      
                        } 
                        else if((cpepIdStringMap.get(cpep.Id)==NULL)||((cpepIdStringMap.get(cpep.Id)!=NULL)&&
                            !(cpepIdStringMap.get(cpep.Id).contains(OFFLABEL)))){
                                   onLabelCPEP.put(cpep.Id,cpep);                      
                        }  
            }
        }
        
       ordList=[Select Id,AIP_Affiliated_Program__c,AIP_Care_Program_Enrollee_Product__c from Order 
                            where AIP_Care_Program_Enrollee_Product__c IN:cpepIdStringMap.keyset() LIMIT 49999];
       
           
        if(!ordList.isEmpty()){
            for(Order ord:ordList){
                if((!offLabelCPEP.keyset().isEmpty())&&(offLabelCPEP.keyset().contains(ord.AIP_Care_Program_Enrollee_Product__c))){
                    ord.AIP_Affiliated_Program__c=OFFLABEL;
                    updateList.add(ord);
                }else if((!onLabelCPEP.keyset().isEmpty())&&(onLabelCPEP.keyset().contains(ord.AIP_Care_Program_Enrollee_Product__c))){
                    ord.AIP_Affiliated_Program__c=ONLABEL;
                	updateList.add(ord);
                }
            }
        }
        
        if(!updateList.isEmpty()){
            database.update(updateList,true);
        }
        
        if(!accIdSet.isEmpty())
        { 
            updatePOC(accIdSet);
        }   
        if(!outreachSiteIdSet.isEmpty())
        {
            createOutreachTasks(outreachSiteIdSet);
        }
        }Catch(Exception e){    
            APS_LogExceptionEventCls.logExceptionEvent(e,CLASSNAME,CPEPUPDATEOPERATIONS);
        }
    }
        /*
************************************************************************************************************************************
@Method Name: cpepUpdateOperations
@description : This method will update Alexion POC on accounts on CPEP Insert if status is On therapy
@date :  20-Nov-2020
*************************************************************************************************************************************/
    public static void updatePOConAccountonCPEPInsert(List<CareProgramEnrolleeProduct> newCPEPList)
    {
        Set<String> accIdSet = new Set<String>();
        List<Account> accUpdateList = new List<Account>();
        AIP_Indication_Information__c indicationSettings = AIP_Indication_Information__c.getValues(System.Label.AIP_Neurology_and_Complement);
        List<String> indicationValues = new List<String>();
        try{
        if(indicationSettings!=null)
        {
            indicationValues =  indicationSettings.AIP_Indication__c.split(','); 
        }         
        for(CareProgramEnrolleeProduct cpepObj : newCPEPList)
        {
            if(cpepObj.Status == System.Label.AIP_On_Therapy)
            {
                accIdSet.add(cpepObj.AIP_Account__c);
            }
        }
        if(!accIdSet.isEmpty())
        { 
            for(Account accObj : [SELECT Id,AIP_Case_Manager__c,AIP_Patient_Liason__c,AIP_Has_Been_On_Therapy__c,AIP_Indication__c
                                  FROM Account WHERE Id=:accIdSet])
            {
                if(!accObj.AIP_Has_Been_On_Therapy__c)
                {
                    accObj.AIP_Has_Been_On_Therapy__c = TRUE;   
                }
                if(indicationValues.contains(accObj.AIP_Indication__c) && String.isNotBlank(accObj.AIP_Patient_Liason__c))
                {
                    accObj.AIP_Case_Manager__c = accObj.AIP_Patient_Liason__c;
                }
                accUpdateList.add(accObj);
            }
        }  
        if(!accUpdateList.isEmpty())
        {
            database.update(accUpdateList,false);
        }  
        }Catch(Exception e){    
            APS_LogExceptionEventCls.logExceptionEvent(e,CLASSNAME,UPDATEPOCONACCOUNT);
        }
    }
             /*
************************************************************************************************************************************
@Method Name: createOutreachTasks
@description : This method will be used to create site outreach tasks on CPEP updatea
@date :  30-Nov-2020
*************************************************************************************************************************************/   
    public static void createOutreachTasks(Set<Id> outreachSiteIdSet)
    {
        try{
        //System.debug('Inside createOutreachTasks='+outreachSiteIdSet);
        //Map<String,APS_Task__c> taskMap = new Map<String,APS_Task__c>();
        Map<Id,Account> accMap = new Map<Id,Account>();
        List<APS_Task__c> taskList = new List<APS_Task__c>();
        for(APS_Task__c tsk:[SELECT Id,Name, AIP_Account_Name__c,AIP_Account_Name__r.AIP_Closed_Facility__c,APS_Status__c
                             FROM APS_Task__c 
                             WHERE AIP_Account_Name__c=:outreachSiteIdSet
                             AND RecordType.DeveloperName='AIP_Site_Outreach'])
        {
            if(outreachSiteIdSet.contains(tsk.AIP_Account_Name__c) && 
               ((tsk.APS_Status__c!='Completed' && tsk.APS_Status__c!='Cancelled') || tsk.AIP_Account_Name__r.AIP_Closed_Facility__c))
            {
                outreachSiteIdSet.remove(tsk.AIP_Account_Name__c);   
            }  
        }
        for(Account acc:[SELECT Id,AIP_COR_User__c,AIP_Outreach_Frequency__c,AIP_Closed_Facility__c from Account 
                         WHERE Id=:outreachSiteIdSet])
        {
            accMap.put(acc.Id, acc);
            if(outreachSiteIdSet.contains(acc.Id) && acc.AIP_Closed_Facility__c)
            {
                outreachSiteIdSet.remove(acc.Id);   
            }
        }
       // System.debug('accMaptoAssign='+accMap);
        for(Id accId:outreachSiteIdSet)
        {
            APS_Task__c newTask = new APS_Task__c();
            newTask.RecordTypeId = Schema.SObjectType.APS_Task__c.getRecordTypeInfosByDeveloperName().get('AIP_Site_Outreach').getRecordTypeId();
            newTask.AIP_Account_Name__c = accId;
            newTask.APS_Subject__c = System.Label.AIP_Site_Outreach;
            if(accMap.get(accId).AIP_COR_User__c!=NULL)
                newTask.OwnerId = accMap.get(accId).AIP_COR_User__c;
            if(accMap.get(accId).AIP_Outreach_Frequency__c!=NULL)
            {
                 newTask.APS_Task_Due_Date__c = Date.today().addDays(Integer.valueOf(accMap.get(accId).AIP_Outreach_Frequency__c));
                 newTask.APS_Original_Due_Date__c = Date.today().addDays(Integer.valueOf(accMap.get(accId).AIP_Outreach_Frequency__c));
            }
            else
            {
                 newTask.APS_Task_Due_Date__c = Date.today().addDays(30);
                 newTask.APS_Original_Due_Date__c = Date.today().addDays(30);
            }
            taskList.add(newTask);
        }
        //System.debug('taskList-->'+taskList);
        if(!taskList.isEmpty())
        {
            database.insert(taskList,false);
        }
        }Catch(Exception e){    
            APS_LogExceptionEventCls.logExceptionEvent(e,CLASSNAME,CREATEOUTREACHTASKS);
        }
    }
            /*
************************************************************************************************************************************
@Method Name: updatePOC
@description : This method will update Alexion POC on accounts on CPEP Update
@date :  20-Nov-2020
*************************************************************************************************************************************/
    public static void updatePOC(Set<String> accIdSet)
    {
        try{
        List<Account> accUpdateList = new List<Account>();     
        AIP_Indication_Information__c indicationSettings = AIP_Indication_Information__c.getValues(System.Label.AIP_Neurology_and_Complement);
        List<String> indicationValues = new List<String>();
        if(indicationSettings!=null)
        {
            indicationValues =  indicationSettings.AIP_Indication__c.split(','); 
        }         
            System.debug('Inside update POC' +accIdSet);
        if(!accIdSet.isEmpty())
        { 
            for(Account accObj : [SELECT Id,AIP_Case_Manager__c,AIP_Patient_Liason__c,AIP_Has_Been_On_Therapy__c,AIP_Indication__c
                                  FROM Account WHERE Id=:accIdSet LIMIT 49999])
            {
                if(!accObj.AIP_Has_Been_On_Therapy__c)
                {
                    accObj.AIP_Has_Been_On_Therapy__c = TRUE;   
                }
                if(indicationValues.contains(accObj.AIP_Indication__c) && String.isNotBlank(accObj.AIP_Patient_Liason__c))
                {
                    accObj.AIP_Case_Manager__c = accObj.AIP_Patient_Liason__c;
                }
                accUpdateList.add(accObj);
            }
            if(!accUpdateList.isEmpty())
            {
                database.update(accUpdateList,false);
            }  
        }
        }Catch(Exception e){    
            APS_LogExceptionEventCls.logExceptionEvent(e,CLASSNAME,UPDATEPOC);
        }
    }
    /*
*****************************************************************************************
Method Name : throwValidationOnTherapyCPEP
Passing Parameters : 
Description : This method is used to throw Validation when a new CPEP is being created 
if there is already an existing CPEP with status of On Therapy.
Author : 
*****************************************************************************************
*/
    public static void throwValidationOnTherapyCPEP(Set<Id> cpeIds,Map<Id,CareProgramEnrolleeProduct> cpepMap){
        set<Id> existingCpeIds = new set<Id>();
        try{
                for(CareProgramEnrolleeProduct cpepRec : [SELECT Id,CareProgramEnrolleeId 
                                                                 FROM CareProgramEnrolleeProduct
                                                                 WHERE CareProgramEnrolleeId IN : cpeIds
                                                                 AND Status = 'On Therapy' AND Id NOT IN:cpepMap.keyset()
                                                                 LIMIT 49999]) {
                    existingCpeIds.add(cpepRec.CareProgramEnrolleeId);
                }
            
            if(!cpepMap.isEmpty()){
                for(CareProgramEnrolleeProduct cpepRec : cpepMap.values()){
                    if(existingCpeIds.contains(cpepRec.CareProgramEnrolleeId)) {
                        if('On Therapy'.equals(cpepRec.Status)){
                        	cpepRec.addError('There should be only one CPEP with On therapy Status');
                        }    
                    }
                }
            }
        }Catch(Exception e){    
            APS_LogExceptionEventCls.logExceptionEvent(e,'AIP_CPEP_Helper','throwValidationOnTherapyCPEP');
        }
    }
}