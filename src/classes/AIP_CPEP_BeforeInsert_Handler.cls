public with sharing class AIP_CPEP_BeforeInsert_Handler implements TriggerFramework.IHandler{
    private static final string THROWCPEP ='throwErrorForDuplicateCPEP';
    private static final string CLASSNAME='AIP_CPEP_BeforeInsert_Handler';
    private static final string POPULATESTARTDATE ='populateStartDate';
    
    public void execute(TriggerFramework.Context context) {
        List<CareProgramEnrolleeProduct> newCPEPList  = (List<CareProgramEnrolleeProduct>)context.newList;
        try{
            TriggerFrameworkMethodUtility.retrieveMethodDetails();
            throwErrorForDuplicateCPEP(newCPEPList);
            updateOffLabelFlag(newCPEPList);
            populateStartDate(newCPEPList);
        }catch(exception c){
            APS_LogExceptionEventCls.logExceptionEvent(c, 'AIP_CPEP_BeforeInsert_Handler', 'execute');
        }
    }
    
    public static void throwErrorForDuplicateCPEP(List<CareProgramEnrolleeProduct> newCPEPEList){
        
        Map<Id,Id> cpeId=new Map<Id,Id>();
        Map<Id,CareProgramEnrolleeProduct> existingCPEP=new Map<Id,CareProgramEnrolleeProduct>();
        try{
            for(CareProgramEnrolleeProduct cpepRec : newCPEPEList){
                cpeId.put(cpepRec.CareProgramEnrolleeId,cpepRec.CareProgramProductId);
            }
            for(CareProgramEnrolleeProduct cpep:[SELECT Id,CareProgramEnrolleeId,CareProgramProductId from
                                                 CareProgramEnrolleeProduct where CareProgramEnrolleeId IN:cpeId.keyset() 
                                                 LIMIT 50000]){
                                                     existingCPEP.put(cpep.CareProgramEnrolleeId,cpep);
                                                 }
            
            if(!existingCPEP.isEmpty()){
                for(CareProgramEnrolleeProduct cpepRec : newCPEPEList){
                    if((existingCPEP.keyset().contains(cpepRec.CareProgramEnrolleeId)) && (cpepRec.CareProgramProductId==(existingCPEP.get(cpepRec.CareProgramEnrolleeId).CareProgramProductId))){
                        cpepRec.addError('There is already a Care program enrollee product assoicated with the account'); 
                    }
                }
            }
        }Catch(Exception e){    
            APS_LogExceptionEventCls.logExceptionEvent(e,CLASSNAME,THROWCPEP);
        }
    }
    public static void updateOffLabelFlag(List<CareProgramEnrolleeProduct> newCPEPList)
    {
        List<CareProgramEnrolleeProduct> cpepList=new List<CareProgramEnrolleeProduct>();
        Set<Id> setAccount=new Set<Id>();
         Map<Id, Account> mapAccount=new Map<Id, Account>();
        Id OrderOnlyRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AIP_Order_Only').getRecordTypeId();
        try{
            for(CareProgramEnrolleeProduct cpep: newCPEPList)
            {
                setAccount.add(cpep.AIP_Account__c);
            }
            if(!setAccount.isEmpty()) {   
               mapAccount=new Map<Id, Account>([select id, aip_off_label_flag__c, recordtypeid,RecordType.Name from Account where ID IN:setAccount]);
            }
            if(!mapAccount.isEmpty())
            {
                for(careProgramEnrolleeProduct cpep: newCPEPList)
                {
                    cpep.AIP_Account_Record_Type__c=mapAccount.get(cpep.AIP_Account__c).RecordType.Name;
                    if(mapAccount.get(cpep.AIP_Account__c).AIP_Off_Label_Flag__c != null && mapAccount.get(cpep.AIP_Account__c).recordtypeid==OrderOnlyRecordTypeId)
                        cpepList.add(cpep);
                }
                if(!cpepList.isEmpty())
                {
                    for(CareProgramEnrolleeProduct cpep:cpepList)
                    {
                        cpep.AIP_Off_Label_Flag__c=mapAccount.get(cpep.AIP_Account__c).AIP_Off_Label_Flag__c;
                    }
                    
                }
            }
        }Catch(Exception e){    
            APS_LogExceptionEventCls.logExceptionEvent(e,'AIP_CPEP_AfterInsert_Handler','updateOffLabelOnCPEP');
        }
        
    }
    public static void populateStartDate(List<CareProgramEnrolleeProduct> newCPEPEList)
    {
        try{
            for(CareProgramEnrolleeProduct cpepRec : newCPEPEList)
            {
                if(cpepRec.AIP_Therapy_Start_Date__c!=NULL)
                {
                    cpepRec.AIP_Start_Date__c = cpepRec.AIP_Therapy_Start_Date__c;
                }
            }
        }Catch(Exception e){    
            APS_LogExceptionEventCls.logExceptionEvent(e,CLASSNAME,POPULATESTARTDATE);
        }
    }
    
}