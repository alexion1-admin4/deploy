@isTest
public class AIP_CPEP_AfterUpdate_Handler_Test1 {
    
   @testSetup
    private static void test() {
         APS_DataUtility uData = new APS_DataUtility();
        
        Account patacc = uData.createpatientAccount();
        patacc.AIP_Active_HIPAA_Consent__c = TRUE;
        Account hcpAccountObj = uData.createHCPAccount();
        Id OrderOnlyRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AIP_Order_Only').getRecordTypeId();
        Account acc = new Account();
        acc.name = 'test234';
        acc.RecordTypeId = OrderOnlyRecordTypeId;
        acc.AIP_Indication__c = 'HPP';
        List<Account> accList = new List<Account>{patacc,hcpAccountObj};
        Database.insert(accList);
        
        CareProgram programObj = uData.createProgram('HPP');
        programObj.Name='HPP';
        Database.Insert(programObj);
        
        CareProgramEnrollee programAffObj = uData.createProgramAff(patacc, hcpAccountObj, programObj);
        programAffObj.CareProgramId=programObj.id;
        database.insert(programAffObj,true);
        
        Product2 solirisProductObj = uData.createProduct('Soliris');
        Product2 kanumaProductObj = uData.createProduct('Kanuma');
        Product2 ultomirisProductObj = uData.createProduct('Ultomiris');
        List<Product2> productList = new List<Product2>{solirisProductObj,kanumaProductObj,ultomirisProductObj};
        database.insert(productList,true);
        System.debug('productList '+productList);
        System.debug('productList 0 '+solirisProductObj);
        System.debug('productList 1 '+kanumaProductObj);
        System.debug('productList 2 '+ultomirisProductObj);
        
       
        CareProgramProduct kanumaprgPrdAffObj = uData.createPrgPrdAff(programObj, productList[1]);
        kanumaprgPrdAffObj.Name = 'Kanuma';
        CareProgramProduct ultomrisprgPrdAffObj = uData.createPrgPrdAff(programObj, productList[2]);
        ultomrisprgPrdAffObj.Name = 'Ultomiris';
        List<CareProgramProduct> careProgramProductList = new List<CareProgramProduct>{kanumaprgPrdAffObj,ultomrisprgPrdAffObj};
        database.insert(careProgramProductList,true);
        
         CareProgramProduct solirisprgPrdAffObj = uData.createPrgPrdAff(programObj, productList[0]);
        solirisprgPrdAffObj.Name = 'Soliris';
        
        insert solirisprgPrdAffObj;
       CareProgramEnrolleeProduct CPEPS= new CareProgramEnrolleeProduct(Name='Test CPEP',
                                                                        CareProgramEnrolleeId=programAffObj.id,
                                                                        CareProgramProductId =solirisprgPrdAffObj.id,
                                                                        AIP_Account__c=acc.id
                                                                        //AIP_Origin__c='COR',
                                                                       // Status = 'Treatment Coordination'
                                                                            );                                                                     
           
        insert CPEPS;  
        cpeps.Status = 'Treatment Coordination';
        Update CPEPS;
        cpeps.Status = 'On Therapy';
        cpeps.AIP_First_Shipment_Date__c=system.today();
        Update CPEPS;
        
        List<CareProgramEnrolleeProduct> careProgramProdList = new List<CareProgramEnrolleeProduct>();
        for(Integer i=0; i<careProgramProductList.size();i++)
        {
        CareProgramEnrolleeProduct CPEP= new CareProgramEnrolleeProduct(Name='Test CPEP'+careProgramProductList[i].Name,
                                                                        CareProgramEnrolleeId=programAffObj.id,
                                                                        CareProgramProductId =careProgramProductList[i].id,
                                                                        AIP_Account__c=patacc.id
                                                                        //AIP_Origin__c='COR',
                                                                        //Status = 'Treatment Coordination'                                                                     
                                                                        
                                                                       );
		careProgramProdList.add(CPEP);            
        }
        system.debug('list123' +careProgramProdList);
        Database.insert(careProgramProdList, true);
        List<CareProgramEnrolleeProduct> careProgramProdListToUpdate = new List<CareProgramEnrolleeProduct>();
        for(Integer i=0; i<careProgramProdList.size();i++)
        {
       	CareProgramEnrolleeProduct CPEP = new CareProgramEnrolleeProduct();
        CPEP.Id= careProgramProdList[i].Id;
        CPEP.Status='Treatment Coordination';
            if(String.ValueOf(careProgramProdList[i].name).Contains('Ultomiris'))
            {
                CPEP.AIP_Last_known_Infusion_Date__c=date.today().addDays(-50);
            }else
            {
                CPEP.AIP_Last_known_Infusion_Date__c=date.today().addDays(-10);                
            }
        CPEP.AIP_Status_Reason__c='Shipped';
        CPEP.AIP_First_Shipment_Date__c = system.today();        
        careProgramProdListToUpdate.add(CPEP);
        }
        UPDATE careProgramProdListToUpdate;
        AIP_Event__c event1 = new AIP_Event__c();
        event1.AIP_Care_Program_Enrollee_Product__c = careProgramProdListToUpdate[0].id;
        insert event1;
        
   
        
    }
    
    static testmethod void testcreateCompetitiveAndOtherHistoryRecordmethod(){
        
        CareProgramEnrolleeProduct carecpep = [select id ,name,CareProgramEnrolleeId,AIP_Account__c,AIP_Next_Therapy_Type__c,AIP_Therapy2_Name__c from CareProgramEnrolleeProduct WHERE AIP_Account__c!=null AND (CareProgramProduct.Name='Soliris' OR CareProgramProduct.Name='Ultomiris') limit 1];
        carecpep.Status='Treatment Coordination';
        carecpep.AIP_Off_Label_Flag__c='On-Label';
        carecpep.AIP_Origin__c='Emergent';
        //carecpep.AIP_Status_Reason__c='End of Life';        
        //carecpep.AIP_Next_Therapy_Type__c ='Other Therapy';
        //carecpep.AIP_Therapy2_Name__c ='BMT';
        
        test.startTest();
        update carecpep;
        test.stopTest();
        //APS_Treatment_History__c apstreatment = [select id ,AIP_Care_Program_Enrollee_Product__r.id,APS_Patient__c,AIP_First_Shipment_Date__c,AIP_Active__c,AIP_Subtype__c from APS_Treatment_History__c where AIP_Care_Program_Enrollee_Product__r.id =:carecpep.id ];
       // system.assert(apstreatment.Id!= null);
    }
    
    static testmethod void testcreateCompetitiveAndOtherHistoryRecordmethod2(){
        CareProgramEnrolleeProduct carecpep = [select id ,name,CareProgramEnrolleeId,AIP_Account__c,AIP_Next_Therapy_Type__c,AIP_Therapy2_Name__c from CareProgramEnrolleeProduct limit 1];
        carecpep.Status='Discontinuation';
        carecpep.AIP_Off_Label_Flag__c='On-Label';
        carecpep.AIP_Has_Been_On_Therapy__c = true;
        carecpep.AIP_Origin__c='Emergent';
        carecpep.AIP_Status_Reason__c='End of Life';        
        carecpep.AIP_Next_Therapy_Type__c ='Other Therapy';
        carecpep.AIP_Therapy2_Name__c ='BMT';
        
        test.startTest();
        update carecpep;
        test.stopTest();
        //APS_Treatment_History__c apstreatment = [select id ,AIP_Care_Program_Enrollee_Product__r.id,APS_Patient__c,AIP_First_Shipment_Date__c,AIP_Active__c,AIP_Subtype__c from APS_Treatment_History__c where AIP_Care_Program_Enrollee_Product__r.id =:carecpep.id ];
       // system.assert(apstreatment.Id!= null);
    }
    
    
    
}