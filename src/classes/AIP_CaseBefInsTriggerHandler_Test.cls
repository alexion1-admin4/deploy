@isTest(SeeAllData=false)
private with sharing class AIP_CaseBefInsTriggerHandler_Test {
Private static User thisUser = [select Id from User where Id = :UserInfo.getUserId() limit 1];
    @isTest static void testTaskClosureAfterCaseClosure() {
        System.runAs(thisUser){ 
        APS_DataUtility dataUtilityObj = new APS_DataUtility();
        dataUtilityObj.insertingCustomSettingData();
        List<Account> accList = new List<Account>();
        Account patientAccount = dataUtilityObj.createpatientAccount();
          AccList.add(patientAccount);
       // Database.insert(patientAccount,true);
        APS_Zip_Code_Master__c zipCodeMasterObj = dataUtilityObj.createZipCode('60690');
        Database.insert(zipCodeMasterObj,true);
          Account hcpAccountObj = dataUtilityObj.createHCPAccount();
          AccList.add(hcpAccountObj);
                  Database.insert(AccList,true);
        
        
        CareProgram programObj = dataUtilityObj.createProgram('HPP');
        Database.insert(programObj,true);
        Product2 productObj = dataUtilityObj.createProduct('Strensiq');
        Database.insert(productObj,true);

        CareProgramEnrollee prgAff1 = dataUtilityObj.createProgramAff(patientAccount,hcpAccountObj,programObj);
        Database.insert(prgAff1,true);
        
        
        CareProgramProduct prgPrdAffObj = dataUtilityObj.createPrgPrdAff(programObj, productObj);
        prgPrdAffObj.Name = 'Hpp';
        Database.insert(prgPrdAffObj,true);
            
        APS_Workflow_Master__c workflowMasterObj = new APS_Workflow_Master__c(
        Name = 'AAF',
        APS_Release_Start_Date__c = Date.Today()-10,
        APS_Release_End_Date__c = Date.Today()+10,
        APS_Workflow_Type__c = 'AAF',
        APS_Description__c = 'Hpp',
        APS_Record_Type__c = 'AAF'
        );
        Database.insert(workflowMasterObj,true);
        
        APS_Program_Product_WF_Aff__c prgPrdWFAffObj = new APS_Program_Product_WF_Aff__c(
        Name = 'Hpp',
        APS_Workflow__c = workflowMasterObj.Id,
        APS_Care_Program_Product_Aff__c = prgPrdAffObj.Id
        );
        Database.insert(prgPrdWFAffObj,true);
        APS_Program_Product_WF_Version__c prgPrdWFVer = new APS_Program_Product_WF_Version__c(
        APS_Program_Product_WF_Aff__c = prgPrdWFAffObj.Id,
        APS_Release_End_Date__c = Date.Today()+10,
        APS_Release_Start_Date__c =Date.Today()-10,
        Name='Version 1'    
        );
        Database.insert(prgPrdWFVer,true);
        CareProgramEnrolleeProduct CPEP= new CareProgramEnrolleeProduct(Name='Test CPEP',
                                                                       AIP_Account__c=patientAccount.id,
                                                                       CareProgramEnrolleeId=prgAff1.id,
                                                                       CareProgramProductId =prgPrdAffObj.id,
                                                                       //AIP_Status_Reason__c='Shipped',
                                                                       AIP_First_Shipment_Date__c=Date.newInstance(2020, 5, 7));
              database.insert(CPEP,true);
                 
 Test.startTest();
    Case csObj = dataUtilityObj.createCase(patientAccount, prgAff1, hcpAccountObj, programObj, prgPrdAffObj,APS_Util_Constants.ONBOARDING);
        csObj.AIP_isfirstcase__c = True;
        csObj.Status = 'new';
        csObj.APS_Account__c = patientAccount.id;
        csObj.RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('AAF').getRecordTypeId();
        Database.insert(csObj,true);
        
        Case csObj1 = dataUtilityObj.createCase(patientAccount, prgAff1, hcpAccountObj, programObj, prgPrdAffObj,APS_Util_Constants.ONBOARDING);
        csObj1.AIP_isfirstcase__c = True;
        csObj1.Status = 'new';
        csObj1.APS_Account__c = patientAccount.id;
        csObj1.RecordTypeId=Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('AAF').getRecordTypeId();
        Database.insert(csObj1,false);
        APS_Task__c apstsk = new APS_Task__c(
                                               APS_Subject__c='AdherenceTask',
                                               APS_Priority__c='Normal',
                                               AIP_Attempt__c = 1,
                                               APS_Status__c=APS_Util_Constants.ASSIGNED,
                                               APS_Sequence__c=1,
                                               APS_Primary_Contact_Welcome__c='Patient',
                                               AIP_Outreach_Channel__c ='Mail',
                                          	   AIP_Outreach_Outcome__c='Not Needed',
                                               AIP_Set_Risk__c ='Moderate',
                                              APS_Case__c = csObj.Id);
      Database.Insert(apstsk,true);
        
        csObj.AIP_Careplan_Case_Created__c = 'yes';        
        csObj.AIP_Status_Reason__c = 'Completed';
        csObj.Status = 'Closed';
        try{
          Database.update(csObj,true);
          
        //AIP_CaseAfterUpdateTriggerHandler.cancelAllPendingTasks(null);
        Test.stopTest();
        //Database.insert(ctmList,true);
        }catch(exception e ){ e.getMessage(); }
        System.assert(true, 'testTaskClosureAfterCaseClosure method pass');
        }
      }
}