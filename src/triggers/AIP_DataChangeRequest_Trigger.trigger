Trigger AIP_DataChangeRequest_Trigger on AIP_Data_Change_Request__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	TriggerFramework.handle();
}