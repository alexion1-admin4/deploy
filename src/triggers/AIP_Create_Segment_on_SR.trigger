/*
 * Survey-Segment Module for Annual Verification Case
 * Ajay Singh- 6th Oct,2021
 */
trigger AIP_Create_Segment_on_SR on APS_Survey_Response__c (After update) {
    
    List<APS_Survey_Response__c> SrLst = [select id,APS_Risk_Counter__c,APS_SurveyName__c,APS_Task__c
                                          from APS_Survey_Response__c where
                                          id in : trigger.new and APS_Risk_Counter__c!=null];
    list<APS_Program_Product_Setting__c> PPSLst = [select id,APS_Higher_Range_Value__c,APS_Lower_Range_Value__c,
                                                   APS_Survey_Name__c,APS_Risk_Segment__c from APS_Program_Product_Setting__c
                                                   where RecordType.name='General Settings' limit 49999
                                                  ];
    list<APS_Task__c> tsklst = new list<APS_Task__c>();
    for (APS_Survey_Response__c sr :SrLst){
        for(APS_Program_Product_Setting__c ps : PPSLst){
            if(sr.APS_SurveyName__c==ps.APS_Survey_Name__c){
                if(sr.APS_Risk_Counter__c >= ps.APS_Lower_Range_Value__c &&
                   sr.APS_Risk_Counter__c <= ps.APS_Higher_Range_Value__c ){
                       APS_Task__c tsk = new APS_Task__c(id=sr.APS_Task__c);
                       tsk.AIP_Survey_Score__c=sr.APS_Risk_Counter__c;
                       if(sr.APS_Risk_Counter__c!=0){
                           tsk.AIP_Survey_Segment__c=ps.APS_Risk_Segment__c ;
                       }
                       tsklst.add(tsk);
                       break;
                   }
            }
        }
        
    }
    update tsklst;
}