#!/bin/bash

mecho "start"
WSPACE="`pwd`"
echo Workspace: $WSPACE
export TARGET=$WSPACE/DeployPackage/src/
GitCommitID="1e39b6fd9480045e30a3c4ef8f6dadeb8f2b574d"
git diff-tree -r --no-commit-id --name-only --diff-filter=ACMRT ${GitCommitID} HEAD | xargs -d '\n' git checkout-index -f --prefix='SalesforceDelta/'
echo $GitCommitID
cd SalesforceDelta/

find ./src -type f > modifiedComponents.txt
mv modifiedComponents.txt ../

find ./src -mindepth 2 -maxdepth 3 -type d > directories.txt
mv directories.txt ../
cd ..
while IFS= read -r line; do echo $line; done < modifiedComponents.txt
mkdir DeployPackage -p
META_DATA_SUFIX="-meta.xml"
while IFS= read -r line; do
cp -R --parents "$line" ./DeployPackage/ || true
cp -R --parents "$line""$META_DATA_SUFIX" ./DeployPackage/ || true
done < modifiedComponents.txt
while IFS= read -r directoryMeta; do
cp -R --parents "$directoryMeta""$META_DATA_SUFIX" ./DeployPackage/ || true
done < directories.txt
echo "-------Meta-Files Copied to DeployPackage--------"
echo "Target folder for deployment is"$TARGET
cp -v package.xml ${TARGET}
echo "package.xml copied into Target directory"